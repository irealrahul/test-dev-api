const express = require("express");
const errorHandler = require("./middleware/errorHandler");
const dotenv = require("dotenv").config();
const cors = require("cors");
const client = require("./DB/faunadb")


const app = express();

const port = process.env.PORT || 6000


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


//middleware for faunadb
app.use("/api", require("./routes/faunadb"));


app.use(errorHandler);



app.listen(port, () => {
  console.log(`server is running ${port}`);
});
