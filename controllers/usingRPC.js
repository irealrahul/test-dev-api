const { ethers } = require("ethers");
const { BigNumber } = require('bignumber.js');


// http://localhost:6001/api/get/rpc/local
// @post

const getRpc = async(req,res)=>{
    const {endpoint, toaddress, fromaddress, amount, privateKey } = req.body

const provider = new ethers.providers.JsonRpcProvider(endpoint)
const chainId = await provider.getNetwork().then((network) => network.chainId);
    console.log('Chain ID:',chainId);
    const networkName = await provider.getNetwork().then((network) => network.name);
console.log("network name",networkName)

    const wallet = new ethers.Wallet(privateKey,provider);
    const txData = {
        nonce: null,
        gasPrice: ethers.utils.parseUnits("50", "gwei"),
        gasLimit: 21000,
        to: toaddress,
        from:fromaddress,
        value: ethers.utils.parseEther(amount),
        data: "0x",
      };

    //   console.log("wallet",wallet)
    //   console.log("txData",txData)
      const balance = await provider.getBalance(wallet.address)
      console.log("balance",balance)
      async function getRawTransaction() {
     
        try {
          const balance = await provider.getBalance(wallet.address);
          const gasCost = txData.gasPrice.mul(txData.gasLimit);
          const totalCost = gasCost.add(txData.value);
          const totalCostInNumber = new BigNumber( gasCost.add(txData.value)._hex).toNumber()
          console.log(totalCostInNumber)
      
          if (balance.lt(totalCost)) {
            console.error("Insufficient balance in the wallet.");
            res.send({error:"Insufficient balance in the wallet."})
            return;
          }else{
      
         // txData.nonce = await wallet.getTransactionCount();
      
          const transaction = {
            ...txData,
            chainId:chainId, //  (MATIC:80001) (BNB: 97) (ETH:5)
          };
      
          const signedTransaction = await wallet.signTransaction(transaction);
          const rawTransaction = ethers.utils.hexlify(signedTransaction);
      
          console.log("Raw Transaction:", rawTransaction);
      
          const tx = await provider.sendTransaction(rawTransaction);
          console.log("Transaction Hash:", tx.hash);
      
      
         const receipt = await provider.waitForTransaction(tx.hash);
          res.send(rawTransaction)
        //   console.log("Transaction Data:", {
        //     ...txData,
        //     txhash: tx.hash,
        //   });
         }

        } catch (error) {
            console.error("Transaction Error:", error);
            res.send({error})
           
          }
    }

    getRawTransaction()
}


module.exports = {getRpc}