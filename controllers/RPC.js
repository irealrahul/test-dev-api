const { ethers } = require("ethers");
const { fetchCollectionData } = require("./FaunaDB/select")
const { client, q } = require("../DB/faunadb")
const { BigNumber } = require('bignumber.js');
const { Transaction } = require('ethereumjs-tx');
const { keccak256 } = require('ethereumjs-util');
let getWalletData = []

// http://localhost:6001/api/set/rpc/local
// @post

const setRPC = async (req, res) => {
    const { jsonrpc, method, params, id } = req.body;

    // geting  'Wallets' full data from db;

    await fetchCollectionData('Wallets')
        .then((data) => {

            getWalletData = [...data];
        })

    // console.log(getWalletData)
    //res.send(getWalletData)
    // End geting  'Wallets' full data from db;
    // blocknumbner 



    // end  


    // decode transaction   
    function decodeRawTransaction(para) {
        try {
            const tx = new Transaction(Buffer.from(para.slice(2), 'hex'), { chain: 'goerli' });
            const chainId = Math.floor((tx.v - 35) / 2);

            tx._chainId = chainId;
            tx._homestead = true;
            tx._homestead = tx._homestead || (tx._chainId > 0);
            tx._homestead = tx._homestead || (tx._chainId === 0);
            tx._homestead = tx._homestead || !!tx._homestead;
            tx._homestead = tx._homestead || false;

            const gasPriceHex = '0x' + tx.gasPrice.toString('hex');
            const gasLimitHex = '0x' + tx.gasLimit.toString('hex');

            const gasPriceWei = new BigNumber(tx.gasPrice.toString('hex'), 16);
            const gasPrice = gasPriceWei / (new BigNumber(10).pow(9)).toNumber();

            const gasLimit = new BigNumber(gasLimitHex).toNumber();
            const signedTxData = tx.serialize();
            const transactionHash = '0x' + keccak256(signedTxData).toString('hex');
            const decodedTx = {
                to: '0x' + tx.to.toString('hex'),
                from: '0x' + tx.getSenderAddress().toString('hex'),
                value: parseFloat(ethers.utils.formatEther(tx.value)).toFixed(15),
                gasPrice: gasPrice,
                gasLimit: gasLimit,
                data: '0x' + tx.data.toString('hex'),
                nonce: parseInt(tx.nonce.toString('hex')),
                v: '0x' + tx.v.toString('hex'),
                r: '0x' + tx.r.toString('hex'),
                s: '0x' + tx.s.toString('hex'),
                Txhash: transactionHash
            };

            return decodedTx;
            // console.log(decodedTx)



        } catch (error) {
            console.error("Error decoding transaction:", error);
            return null;
        }

    }


    // end   
    const chainId = 123;
    const name = "ETH";
    // const balance = 150;
    const gasPrice1 = ethers.utils.parseUnits("50", "wei")
    const gasPrice = new BigNumber(gasPrice1._hex).toNumber()
    //console.log(gasPrice)
    const resultblock = 9332977
    try {

        if (jsonrpc !== "2.0") {
            return res.status(400).json({ error: "jsonrpc version mismatched" });
        } else {
            if (method === "eth_chainId") {
                res.json({ jsonrpc: "2.0", result: `0x${chainId.toString(16)}`, id: id });

            } else if (method === "net_version") {
                res.json({ jsonrpc: "2.0", result: chainId, id: id });

            } else if (method === "eth_getBalance") {
                let j = 0
                for (let i = 0; i < getWalletData.length; i++) {

                    if (params[0] === getWalletData[i].data.address) {
                        const blncHex = "0x" + getWalletData[i].data.balance.toString(16)
                        res.json({ jsonrpc: "2.0", result: blncHex, id: id });
                    } else {
                        j++

                    }


                }
                // console.log(j)
                if (j >= getWalletData.length) {
                    res.status(400).json({ error: "Invalid address" })
                } else {
                    // console.log("hello")
                }



            } else if (method === "eth_gasPrice") {
                res.json({ jsonrpc: "2.0", result: "0x" + gasPrice.toString(16), id: id });

            }
            else if (method === "eth_blockNumber") {
                res.json({ jsonrpc: "2.0", id: id, result: "0x" + resultblock.toString(16) })
            }
            else if (method === "eth_sendRawTransaction") {
                // const rawTransaction = params[0];
                if (!params[0]) {
                    // console.log("body not found")
                }
                else {



                    const decodedTx = decodeRawTransaction(params[0]);

                    // console.log(decodedTx)

                    const unsuccessfulReasons = [];
                    // console.log("matchigTx",getWalletData)

                    let val=0; let success=0; let nonse =0; let balance=0;
                    for(var i=0;i<getWalletData.length;i++){
                       

                        // console.log(tx)
                        if (getWalletData[i].data.address == decodedTx.from) {
                            // console.log("success")
                            if (getWalletData[i].data.nonce <= decodedTx.nonce && getWalletData[i].data.balance >= decodedTx.value) {
                                console.log("success")
                                success++;

                            } else {
                               if(getWalletData[i].data.nonce > decodedTx.nonce){
                                nonse++;

                                
                               }else if(getWalletData[i].data.balance < decodedTx.value){
                                balance ++;

                               }else{
                                nonse= nonse*0;
                                balance = balance*0;
                               }



                                    
                                    // console.log("nonse error");


                                // } else if (getWalletData[i].data.balance < decodedTx.balance) {
                                //     console.log("balance error");


                                // }





                            }



                            // && tx.data.nonce <= decodedTx.nonce && tx.data.balance >= decodedTx.balance





                         



                        } else {
                            // console.log("Address mismatched")
                            val++;


                            // let reasonmassage;
                            // if(tx.data.address !== decodedTx.from ){
                            //     console.log("address")
                            //     reasonmassage = "address mismatched"

                            // }else if(tx.data.nonce > decodedTx.nonce){
                            //     reasonmassage =  "nonce is low"
                            // }
                            // else if(tx.data.balance < decodedTx.balance){
                            //     reasonmassage = "insufficient balance"
                            // }
                            // console.log("reasonmassage",reasonmassage)
                            // client.query(
                            //     q.Create(q.Collection('Transactions'), { data: { decodedTx,transactionHash: decodedTx.hash, statuss: 'Transaction unSuccessful', reason: reasonmassage } })
                            //   ).then((response) => {
                            //     console.log('Transaction saved to FaunaDB:', response);
                            //     res.status(200).json({ jsonrpc:jsonrpc , id:id,error: {message: reasonmassage, data: decodedTx.data} });
                            //   }).catch((error) => {
                            //     console.error('Error saving transaction to FaunaDB:', error);
                            //     //res.status(500).json({ error: 'Internal server error' });
                            //   });
                        }
                        //    else if(tx.data.nonce > decodedTx.nonce)
                        //    {
                        //     console.log("nonce mismatch")
                        //    }
                        //    else if(tx.data.balance < decodedTx.balance){
                        //     console.log("balance not found")
                        //    }
                        //    else{
                        //     console.log("success")
                        //    }
                        
                    }
                    if(val > getWalletData.length){
                        // console.log("address mismatched")
                        client.query(
                            q.Create(q.Collection('Transactions'), { data: { decodedTx,transactionHash: decodedTx.hash, statuss: 'Transaction unSuccessful', reason: "Address Mismatched" } })
                          ).then((response) => {
                            // console.log('Transaction saved to FaunaDB:', response);
                            res.status(200).json({ jsonrpc:jsonrpc , id:id,error: {message: "Address Mismatched", data: decodedTx.data} });
                          }).catch((error) => {
                            // console.error('Error saving transaction to FaunaDB:', error);
                            //res.status(500).json({ error: 'Internal server error' });
                          });
                    }else{
                        if(success == 1){
                            // console.log("success")
                               client.query(
                                q.Create(q.Collection('Transactions'), { data: { decodedTx, statuss: 'Transaction Successful' } })
                              ).then((response) => {
                                // console.log('Transaction saved to FaunaDB:', response);
                                res.status(200).json({ id:id, jsonrpc:jsonrpc,result : decodedTx.Txhash });
                              }).catch((error) => {
                                // console.error('Error saving transaction to FaunaDB:', error);
                               // res.status(500).json({ error: 'Internal server error' });
                              });

                              client.query(
                                  q.Map(
                                    q.Paginate(q.Match(q.Index('all_wallets'))),
                                    q.Lambda(
                                      ['ref'],
                                      q.Let(
                                        {
                                          wallet: q.Get(q.Var('ref'))
                                        },
                                        q.If(
                                          q.Equals(q.Select(['data', 'address'], q.Var('wallet')), decodedTx.from),
                                          q.Update(
                                            q.Var('ref'),
                                            { data: { balance: q.Add(q.Select(['data', 'balance'], q.Var('wallet')), -decodedTx.value ) } }
                                          ),
                                          null
                                        )
                                      )
                                    )
                                  )
                                )
                                .then((response) => {
                                //   console.log(response);
                                })
                                .catch((err) => {
                                //   console.log(err);
                                });

                        }else{
                            if(nonse == 1){
                                // console.log("nonse error")
                                client.query(
                                    q.Create(q.Collection('Transactions'), { data: { decodedTx,transactionHash: decodedTx.hash, statuss: 'Transaction unSuccessful', reason: "Nonce is not Matching" } })
                                  ).then((response) => {
                                    // console.log('Transaction saved to FaunaDB:', response);
                                    res.status(200).json({ jsonrpc:jsonrpc , id:id,error: {message: "Nonce is not Matching", data: decodedTx.data} });
                                  }).catch((error) => {
                                    // console.error('Error saving transaction to FaunaDB:', error);
                                    //res.status(500).json({ error: 'Internal server error' });
                                  });
                            }else if(balance == 1){
                                // console.log("balance error")
                                client.query(
                                    q.Create(q.Collection('Transactions'), { data: { decodedTx,transactionHash: decodedTx.hash, statuss: 'Transaction unSuccessful', reason: "Balanace is insufficient" } })
                                  ).then((response) => {
                                    // console.log('Transaction saved to FaunaDB:', response);
                                    res.status(200).json({ jsonrpc:jsonrpc , id:id,error: {message: "Balanace is insufficient", data: decodedTx.data} });
                                  }).catch((error) => {
                                    // console.error('Error saving transaction to FaunaDB:', error);
                                    //res.status(500).json({ error: 'Internal server error' });
                                  });
                            }else{
                                // console.log("hello")
                            }
                        }
                    }
                    // console.log("matching", matchingTx)

                }
            }
            else if (method === "eth_getTransactionCount"){
                let j = 0
                for (let i = 0; i < getWalletData.length; i++) {

                    if (params[0] === getWalletData[i].data.address) {
                        const blncHex = "0x" + getWalletData[i].data.nonce.toString(16)
                        res.json({ jsonrpc: "2.0", result: blncHex, id: id });
                    } else {
                        j++

                    }


                }
                // console.log(j)
                if (j >= getWalletData.length) {
                    res.status(400).json({ error: "Invalid address" })
                } else {
                    // console.log("hello")
                }
            }
            else {
                res.status(500).json({
                    jsonrpc: jsonrpc,
                    error: {
                        code: 500,
                        message: "Method not found",
                        data: method
                    },
                    id: id
                });
            }
            
        }

    } catch (error) {
        console.error("Error handling the request:", error);
        //  res.status(500).json({ error: error.message });
    }

}

module.exports = { setRPC }