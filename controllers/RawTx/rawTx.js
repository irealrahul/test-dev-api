const { ethers } = require("ethers");
const { Transaction } = require('ethereumjs-tx');
const { BigNumber } = require('bignumber.js');
const { keccak256 } = require('ethereumjs-util');

const { client, q } = require("../../DB/faunadb")
// http://localhost:5001/api/web
//@method  post

//1. endpoint for polygon
//endpoint = "https://compatible-boldest-diamond.matic-testnet.discover.quiknode.pro/dab313dd5560b5432c0d13650621e1fb2248b94f/"
//2. endpoint for ether
// endpoint ="https://dry-divine-model.ethereum-goerli.discover.quiknode.pro/325017d69355bbe68f8b331ead1ab117af06f9c8"
//3. endpoint for BNB
// endpoint ="https://ancient-blue-borough.bsc-testnet.discover.quiknode.pro/5efeb7fd7c42a7f64ccd71766edae2a9545b88ac/"



let result = [];
const getRawTransactions = async (req, res, next) => {

    const {endpoint, toaddress, fromaddress, amount, privateKey } = req.body;
   

    const provider = new ethers.providers.JsonRpcProvider(endpoint);

    const chainId = await provider.getNetwork().then((network) => network.chainId);
    console.log('Chain ID:',chainId);
    const networkName = await provider.getNetwork().then((network) => network.name);
console.log("network name",networkName)

    const wallet = new ethers.Wallet(privateKey,provider);
    const txData = {
        nonce: null,
        gasPrice: ethers.utils.parseUnits("50", "gwei"),
        gasLimit: 21000,
        to: toaddress,
        from:fromaddress,
        value: ethers.utils.parseEther(amount),
        data: "0x",
      };
      
      async function getRawTransaction() {
     
        try {
          const balance = await provider.getBalance(wallet.address);
          const gasCost = txData.gasPrice.mul(txData.gasLimit);
          const totalCost = gasCost.add(txData.value);
      
          if (balance.lt(totalCost)) {
            console.error("Insufficient BNB balance in the wallet.");
            return;
          }
      
          txData.nonce = await wallet.getTransactionCount();
      
          const transaction = {
            ...txData,
            chainId:chainId, //  (MATIC:80001) (BNB: 97) (ETH:5)
          };
      
          const signedTransaction = await wallet.signTransaction(transaction);
          const rawTransaction = ethers.utils.hexlify(signedTransaction);
      
          console.log("Raw Transaction:", rawTransaction);
      
          const tx = await provider.sendTransaction(rawTransaction);
          console.log("Transaction Hash:", tx.hash);
      
      
          const receipt = await provider.waitForTransaction(tx.hash);
      
          console.log("Transaction Data:", {
            ...txData,
            txhash: tx.hash,
          });
        //   res.send({rawTransaction,receipt,networkName})
      
      
   
function decodeRawTransaction() {
  try {
    const tx = new Transaction(Buffer.from(rawTransaction.slice(2), 'hex'), { chain: networkName });
    // const chainId = Math.floor((tx.v - 35) / 2);

    tx._chainId = chainId;
    tx._homestead = true;
    tx._homestead = tx._homestead || (tx._chainId > 0);
    tx._homestead = tx._homestead || (tx._chainId === 0);
    tx._homestead = tx._homestead || !!tx._homestead;
    tx._homestead = tx._homestead || false;

    const gasPriceHex = '0x' + tx.gasPrice.toString('hex');
    const gasLimitHex = '0x' + tx.gasLimit.toString('hex');

    const gasPriceWei = new BigNumber(tx.gasPrice.toString('hex'), 16);
    const gasPrice = gasPriceWei / (new BigNumber(10).pow(9)).toNumber();

    const gasLimit = new BigNumber(gasLimitHex).toNumber();
   
    const valueFix = new BigNumber(txData.value._hex).toNumber()
                    
                    console.log(valueFix)

                    const signedTxData = tx.serialize();
                    const transactionHash = '0x' + keccak256(signedTxData).toString('hex');
                    const decodedTx = {
                        to: txData.to,
                        from: txData.from,
                        value: valueFix,
                        gasPrice: gasPrice,
                        gasLimit: gasLimit,
                        data: txData.data,
                        nonce: txData.nonce,
                        v: parseInt('0x' + tx.v.toString('hex')),
                        r: '0x' + tx.r.toString('hex'),
                        s: '0x' + tx.s.toString('hex'),
                        amount:amount,
                        Txhash: transactionHash,
                        ChainId:chainId
                    };

  return decodedTx;
    
   
  
  } catch (error) {
    console.error("Error decoding transaction:", error);
    return null;
  }

}

async function fetchCollectionData(collectionName) {
  try {
    const result = await client.query(
      q.Map(
        q.Paginate(q.Documents(q.Collection(collectionName))),
        q.Lambda((x) => q.Get(x)),
      
      )
    );
    return result.data;
  } catch (error) {
    console.error('Error fetching collection data:', error);
    throw error;
  }
}

const collectionName = 'Wallets';

fetchCollectionData(collectionName)
  .then((data) => {
    for (let i = 0; i < data.length; i++) {
      const newobj = data[i].data;
      result = [...result, newobj];
      
    }
    console.log(result)


// // Transaction data find

// let result1= [];
// let alreadyData =[]

// async function fetchCollectionData1(collectionName) {
//     try {
//       const result = await client.query(
//         q.Map(
//           q.Paginate(q.Documents(q.Collection(collectionName))),
//           q.Lambda((x) => q.Get(x))
//         )
//       );
//       return result.data;
//     } catch (error) {
//       console.error('Error fetching collection data:', error);
//       throw error;
//     }
//   }
  
//   const collectionName = 'Transactions';
  
//   fetchCollectionData1(collectionName)
//     .then((data) => {
      
//       for (let i = 0; i < data.length; i++) {
//         const newobj = data[i].data;
//         result1 = [...result1, newobj];
//       }
// console.log(result1)
// for(let j=0;j<result1.length;j++){
//     // alreadyData.push(result1[j].decodedTx.data)
// }
// console.log("already dataaaaaa",alreadyData)
//     })


    // end Tx data find in fdb


    // update

    const addressToUpdate = "";
async function update (collectionName) {
  
      
}

    // end update


//balance get

    const getBalance = async () => {
    
    const provider = new ethers.providers.JsonRpcProvider(endpoint);
    const balance = await provider.getBalance(fromaddress);

    console.log(
        `Balance of ${fromaddress} is ${ethers.utils.formatEther(balance)} ether`
    );
    };
    getBalance()

// end   balance get 

//  for wallet data creation   
         async function walletcreate() {
            const data =  { data: {

                address: fromaddress,
                 balance: ethers.utils.formatEther(balance),
                 nonce: txData.nonce,
              }
            }
            const collectionName = 'Wallets';
    
            client.query(q.Create(q.Collection(collectionName), { data }))
      .then((response) => {
        console.log('Document created:', response);
      })
      .catch((error) => {
        console.log('Error creating document:', error);
      });
           }
// end  for wallet data creation  

        try {
     
          if (!rawTransaction) {
            return res.status(400).json({ error: 'Raw transaction data is missing in the request body.' });
          }
          const decodedTx = decodeRawTransaction(rawTransaction);
      
       
            
        
          const unsuccessfulReasons = [];
      console.log("wllt",result)
      if (result===[]) {
        walletcreate()
      }else{
          const matchingTx = result.filter((tx) => {
          console.log("wallets addr:",tx.address)
            if (tx.address === decodedTx.from) {
              if (tx.balance < decodedTx.amount) {
                unsuccessfulReasons.push('Insufficient balance');
              }
              if (tx.nonce > decodedTx.nonce) {
                unsuccessfulReasons.push('Nonce not found');
              }
              return tx.balance >= decodedTx.amount && tx.nonce <= decodedTx.nonce;
            } else {
              unsuccessfulReasons.push('Address mismatch');
          // walletcreate()
              return false;
            }
          });
        
          if (matchingTx.length > 0) {
            // Successful transaction
            client.query(
              q.Create(q.Collection('Transactions'), { data: { decodedTx, statuss: 'Transaction Successful' } })
            ).then((response) => {
              console.log('Transaction saved to FaunaDB:', response);
              res.status(200).json({ message: 'Transaction decoded and saved successfully.' });
            }).catch((error) => {
              console.error('Error saving transaction to FaunaDB:', error);
              res.status(500).json({ error: 'Internal server error' });
            });
                  
        client.query(
            q.Map(
              q.Paginate(q.Match(q.Index('all_wallets'))),
              q.Lambda(
                ['ref'],
                q.Let(
                  {
                    wallet: q.Get(q.Var('ref'))
                  },
                  q.If(
                    q.Equals(q.Select(['data', 'address'], q.Var('wallet')), decodedTx.from),
                    q.Update(
                      q.Var('ref'),
                      { data: { balance: q.Add(q.Select(['data', 'balance'], q.Var('wallet')), -decodedTx.value ) } }
                    ),
                    null
                  )
                )
              )
            )
          )
          .then((response) => {
            console.log(response);
          })
          .catch((err) => {
            console.log(err);
          });
          } else {
            // Unsuccessful transaction
            client.query(
              q.Create(q.Collection('Transactions'), { data: { decodedTx,transactionHash: decodedTx.hash, statuss: 'Transaction unSuccessful', reason: unsuccessfulReasons.join(', ') } })
            ).then((response) => {
              console.log('Transaction saved to FaunaDB:', response);
              res.status(200).json({ message: 'Transaction decoded and saved successfully.',reason: unsuccessfulReasons.join(', ') });
            }).catch((error) => {
              console.error('Error saving transaction to FaunaDB:', error);
              res.status(500).json({ error: 'Internal server error' });
            });
          }
        }
      
        } catch (error) {
          console.error('Error checking transaction:', error);
          res.status(500).json({ error: 'Internal server error' });
        }
    
   
      
  })
  .catch((error) => {
    console.error('Error:', error);
  });
} catch (error) {
    console.error("Transaction Error:", error);
    res.send({error})
    next()
  }
}

  getRawTransaction();


}




// const endpoint = 'https://dry-divine-model.ethereum-goerli.discover.quiknode.pro/325017d69355bbe68f8b331ead1ab117af06f9c8';

// const provider = new ethers.providers.JsonRpcProvider(endpoint);

// async function getChainId() {
//   try {
//     const chainId = await provider.getNetwork().then((network) => network);
//      console.log('Chain ID:', chainId);
//   } catch (error) {
//     console.error('Error:', error);
//   }
// }

// getChainId();



module.exports = { getRawTransactions }